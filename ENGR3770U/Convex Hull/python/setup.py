from setuptools import setup

setup (
    name            = 'convexHull',
    version         = '0.1',
    description     = 'A solution to the Convex Hull problem using the Monotone Chain Algorithm.',
    url             = 'https://github.com/capncanuck/AlgorithmsProject',
    author          = 'Khalil Fazal',
    author_email    = 'khalil.fazal.0@gmail.com',
    license         = 'GPL',
    packages        = ['convexHull'],
    zip_safe        = False
)
